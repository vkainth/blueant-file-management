# BlueAnt Crude File Management
## Introduction
This project aims to serve as a baseline for uploading and downloading
files from a [Cloudant](https://cloudant.com/) NoSQL database running
on [IBM Bluemix](https://www.ibm.com/cloud-computing/bluemix) by allowing
users to interface with their created database from their own Bluemix
account using [Flask](http://flask.pocoo.org/) and Python.
## Motivation
Created for points towards the completion of an Advanced Databases
Course.
## How to run application
Clone the application from github as usual and run
`pip install -r requirements.txt` in the root
of the project. Further,the user needs to paste their cloudant credentials as detailed
[here](https://github.com/IBM-Bluemix/get-started-python#5-add-a-database)
and [here](https://github.com/IBM-Bluemix/get-started-python#6-use-the-database)
to `vcap-local.json` file located in the application root.
The user should, then, follow one of the following,
depending on how they want to run the application.
* __Locally__
    
    The user needs to change
    `port = int(os.getenv('PORT', 8080))` to `port = int(os.getenv('PORT', 5000))`
    and `app.run(host='0.0.0.0', port=port, debug=True)` to
    `app.run(host='localhost', port=port, debug=True)`
* __On Bluemix__

    After completing the initial few steps, the user should connect
    to Bluemix Cloud Services using [CloudFoundry CLI](https://github.com/cloudfoundry/cli),
    supply an `application name` in the `manifest.yml` file located at
    the root of the directory. Then, the user should issue `cf push` from
    the terminal while at the root of the project's directory.
    If everything was setup correctly, the user should be able to access
    the application from their url from the __Apps__ dashboard.
    
__NOTE__:
>Please follow the fairly detailed guide [here](https://github.com/IBM-Bluemix/get-started-python)
to have absolutely no issues.

__Database Not Found Error__:
>If the application crashes and gives a 404: No database found, create
the database by going into the cloudant service.

## Future Development
Organize the file. Better UI. Maybe implement encryption